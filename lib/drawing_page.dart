import 'dart:async';
import 'dart:math';
// import 'dart:html';

import 'package:arkit_facetrack/sketcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

class DrawnLine {
  final List<Offset> path;
  final Color color;
  final double width;

  DrawnLine(this.path, this.color, this.width);
}

class DrawingPage extends StatefulWidget {
  late final ValueListenable<Tuple3<Offset, bool, bool>> paintCoordinates;

  // ignore: prefer_const_constructors_in_immutables
  DrawingPage({required this.paintCoordinates, super.key});

  @override
  // ignore: library_private_types_in_public_api
  _DrawingPageState createState() => _DrawingPageState();
}

class _DrawingPageState extends State<DrawingPage> {
  final GlobalKey _globalKey = GlobalKey();
  List<DrawnLine> lines = <DrawnLine>[]; // Array to store all lines
  List<DrawnLine> undoLines = <DrawnLine>[]; // Array to store removed lines
  List<DrawnLine> clearedLines = <DrawnLine>[]; // Array to store cleared lines
  DrawnLine? line;
  Color selectedColor = Colors.black;
  double selectedWidth = 5.0;

  final List<Color> colorPalette = [
    const Color(0xFF1ABC9C),
    const Color(0xFF2ECC71),
    const Color(0xFF3498DB),
    const Color(0xFF9B59B6),
    const Color(0xFF2371A5),
    const Color(0xFFF1C40F),
    const Color(0xFFE67E22),
    const Color(0xFFE74C3C),
    const Color(0xFF1E272E),
  ];

  bool paletteOpen = false;

  StreamController<List<DrawnLine>> linesStreamController =
      StreamController<List<DrawnLine>>.broadcast();
  StreamController<DrawnLine> currentLineStreamController =
      StreamController<DrawnLine>.broadcast();

  // Future<void> save() async {
  //   try {
  //     RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject() as RenderRepaintBoundary;
  //     ui.Image image = await boundary.toImage();
  //     ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();
  //     var saved = await ImageGallerySaver.saveImage(
  //       pngBytes,
  //       quality: 100,
  //       name: DateTime.now().toIso8601String() + ".png",
  //       isReturnImagePathOfIOS: true,
  //     );
  //     print(saved);
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future<void> clear() async {
    setState(() {
      // Copy the lines array to undoLines before clearing
      clearedLines = List.from(lines);
      lines = [];
      line = null;
    });
  }

  Future<void> undo() async {
    setState(() {
      if (lines.isNotEmpty) {
        undoLines.add(lines.removeLast());
        line = null;
      } else {
        if (clearedLines.isNotEmpty) {
          lines = List.from(clearedLines);
          clearedLines = [];
          line = null;
        }
      }
    });
  }

  // Redo
  Future<void> redo() async {
    setState(() {
      if (undoLines.isNotEmpty) {
        lines.add(undoLines.removeLast());
        line = null;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    widget.paintCoordinates.addListener(onPointerDraw);
    return Scaffold(
      backgroundColor: Colors.yellow[50],
      body: Stack(
        children: [
          buildAllPaths(context),
          buildCurrentPath(context),
          // buildStrokeToolbar(),
          buildToolbar(),
          buildUndoRedoButtons(),
        ],
      ),
    );
  }

  Widget buildCurrentPath(BuildContext context) {
    return GestureDetector(
      onPanStart: onPanStart,
      onPanUpdate: onPanUpdate,
      onPanEnd: onPanEnd,
      child: RepaintBoundary(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.all(4.0),
          color: Colors.transparent,
          alignment: Alignment.topLeft,
          child: StreamBuilder<DrawnLine>(
            stream: currentLineStreamController.stream,
            builder: (context, snapshot) {
              return CustomPaint(
                painter: Sketcher(
                  lines: line == null ? [] : [line!],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildAllPaths(BuildContext context) {
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.transparent,
        padding: const EdgeInsets.all(4.0),
        alignment: Alignment.topLeft,
        child: StreamBuilder<List<DrawnLine>>(
          stream: linesStreamController.stream,
          builder: (context, snapshot) {
            return CustomPaint(
              painter: Sketcher(
                lines: lines,
              ),
            );
          },
        ),
      ),
    );
  }

  void onPointerDraw() {
    Offset point = widget.paintCoordinates.value.item1;
    bool drawing = widget.paintCoordinates.value.item2;
    bool clearAsked = widget.paintCoordinates.value.item3;

    if (clearAsked) {
      lines = [];
      line = null;
    }

    // Check if a line has been started
    if (line == null) {
      line = DrawnLine([point], selectedColor, selectedWidth);
    } else {
      List<Offset> path = List.from(line!.path)..add(point);

      line = DrawnLine(path, selectedColor, selectedWidth);
      currentLineStreamController.add(line!);
    }

    // At end of drawing, see
    if (line != null && !drawing) {
      lines = List.from(lines)..add(line!);
      linesStreamController.add(lines);
      undoLines = [];
      clearedLines = [];
      line = null;
    }
  }

  void onPanStart(DragStartDetails details) {
    RenderBox box = context.findRenderObject() as RenderBox;
    Offset point = box.globalToLocal(details.globalPosition);
    line = DrawnLine([point], selectedColor, selectedWidth);
  }

  void onPanUpdate(DragUpdateDetails details) {
    RenderBox box = context.findRenderObject() as RenderBox;
    Offset point = box.globalToLocal(details.globalPosition);

    List<Offset> path = List.from(line!.path)..add(point);
    line = DrawnLine(path, selectedColor, selectedWidth);
    currentLineStreamController.add(line!);
  }

  void onPanEnd(DragEndDetails details) {
    lines = List.from(lines)..add(line!);
    linesStreamController.add(lines);
    undoLines = [];
  }

  Widget buildUndoRedoButtons() {
    return Positioned(
      top: 10.0,
      left: 10.0,
      child: Row(
        children: [
          IconButton(
            icon: const Icon(Icons.undo),
            onPressed: undo,
          ),
          IconButton(
            icon: const Icon(Icons.redo),
            onPressed: redo,
          ),
        ],
      ),
    );
  }

  Widget buildToolbar() {
    return Positioned(
      top: 20.0,
      right: 10.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          buildButton(const Color(0xFFAEE3D6), "res/brush.png", clear),
          const Divider(
            height: 10.0,
          ),
          buildButton(const Color(0xFFFC94BC), "res/save.png", () => {}),
          const Divider(
            height: 10.0,
          ),
          buildButton(
              const Color.fromARGB(255, 255, 255, 255),
              "res/colors.png",
              () => {
                    setState(
                      () => paletteOpen = !paletteOpen,
                    )
                  }),
          const Divider(
            height: 20.0,
          ),
          if (paletteOpen)
            for (Color color in colorPalette) buildColorButton(color),
          const Divider(
            height: 20.0,
          ),
        ],
      ),
    );
  }

  Widget buildColorButton(Color color) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: FloatingActionButton(
        heroTag: Random().nextInt(500),
        mini: true,
        backgroundColor: color,
        child: Container(),
        onPressed: () {
          setState(() {
            selectedColor = color;
          });
        },
      ),
    );
  }

  Widget buildButton(Color color, String iconPath, Function() onPressed) {
    return GestureDetector(
      onTap: onPressed,
      child: // Rectangle of the color, color with icon in the middle (icon path) and ronded corners and icon has dropshadow
          Container(
        width: 60.0,
        height: 60.0,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 0.3,
              blurRadius: 2,
              offset: const Offset(1, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: Image.asset(
            iconPath,
            width: 40.0,
            height: 40.0,
          ),
        ),
      ),
    );
  }
}
