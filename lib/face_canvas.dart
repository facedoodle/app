import 'dart:collection';
import 'dart:io';
import 'dart:ui';

import 'package:arkit_facetrack/bt.dart';
import 'package:arkit_facetrack/drawing_page.dart';

import 'package:arkit_plugin/arkit_plugin.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

import 'package:tuple/tuple.dart';
import 'package:vector_math/vector_math_64.dart' hide Colors;

import 'package:moving_average/moving_average.dart';
import 'package:video_player/video_player.dart';

// ignore: constant_identifier_names
const double OPTIMAL_COEFF = 1.145; // optimal coeff for the moving average
// ignore: constant_identifier_names
const int FLOATING_WINDOW_SIZE = 2; // size of the floating window

// Ideal ratios for the moving average
// ignore: constant_identifier_names
const int X_RATIO = 5;
// ignore: constant_identifier_names
const int Y_RATIO = 8;

const bool musicEnabled = true; // set to false to disable music

class FaceCanvasPage extends StatefulWidget {
  const FaceCanvasPage({super.key});

  @override
  FaceCanvasPageState createState() => FaceCanvasPageState();
}

class FaceCanvasPageState extends State<FaceCanvasPage> {
  static const int _faceOrientation = 2;

  late ARKitController arkitController;
  ARKitNode? faceNode, leftEye, rightEye;

  final _pointerLocation = ValueNotifier<Offset>(Offset.zero);
  final drawingData = ValueNotifier<Tuple3<Offset, bool, bool>>(
      const Tuple3(Offset.zero, false, false));
  final _plotterValues =
      ValueNotifier<Tuple2<double, double>>(const Tuple2(-4, -4));
  final clearAsked = ValueNotifier<bool>(false);
  final _plotterConnector = FacePlotter();
  late final ValueListenable<bool> _plotterConnected =
      _plotterConnector.connectedProperty();

  int cooldown = 0;
  //music counter
  int musicCounter = 0;
  //counter for blinking
  int blinkCounter = 0;

  bool faceDetected = false;
  final player = AudioPlayer()..setUrl("asset:res/success.mp3");

  final musics = <String>[
    "asset:res/mi_trackerita.mp3",
    "asset:res/indiana_tracking_jones.mp3",
    "asset:res/the_plotter_in_you.mp3",
  ];
  final player2 = AudioPlayer()..setUrl("asset:res/mi_trackerita.mp3");

  @override
  void dispose() {
    arkitController.dispose();
    super.dispose();
  }

  @override
  void deactivate() {
    super.deactivate();
    _plotterConnector.setXY(0, 0);
    _plotterConnector.disconnect();
  }

  @override
  Widget build(BuildContext context) {
    _plotterValues.addListener(() => _plotterConnector.setXY(
        _plotterValues.value.item1, _plotterValues.value.item2));

    return Scaffold(
        backgroundColor: drawingData.value.item2
            ? Colors.blue
            : const Color.fromARGB(255, 162, 162, 162),
        body: ValueListenableBuilder(
          builder: buildMainUI,
          valueListenable: _plotterConnected,
        ));
  }

  Widget buildMainUI(BuildContext context, bool isConnected, Widget? w) {
    return isConnected
        ? Padding(
            padding: EdgeInsets.only(
                //padding so that the drawing window is 7:4 ratio
                top: 25,
                left: 15,
                right: window.physicalSize.width / window.devicePixelRatio -
                    (window.physicalSize.height * 4 / 7) /
                        window.devicePixelRatio,
                bottom: 15),
            child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    ARKitSceneView(
                        configuration: ARKitConfiguration.faceTracking,
                        onARKitViewCreated: onARKitViewCreated),
                    Visibility(
                        visible: faceDetected,
                        maintainState: true,
                        child: DrawingPage(paintCoordinates: drawingData)),
                    IgnorePointer(
                      ignoring: true,
                      child: CustomPaint(
                          painter: PointerPainter(
                              repaint: _pointerLocation,
                              plotterValues: _plotterValues,
                              faceCanvas: this,
                              drawingData: drawingData),
                          willChange: true,
                          child: Container()),
                    ),
                    if (!faceDetected)
                      const Positioned(
                          top: 50,
                          child: DecoratedBox(
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100)),
                                  color: Colors.white),
                              child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  child: Text(
                                    "No face detected",
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: -1.5),
                                  )))),
                  ],
                )))
        : const Center(
            child: DecoratedBox(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.white),
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: Text(
                      "Connecting to Plotter…",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          letterSpacing: -1.5),
                    ))),
          );
  }

  void onARKitViewCreated(ARKitController arkitController) {
    this.arkitController = arkitController;
    this.arkitController.onAddNodeForAnchor = _handleAddAnchor;
    this.arkitController.onUpdateNodeForAnchor = _handleUpdateAnchor;
  }

  void _handleAddAnchor(ARKitAnchor anchor) {
    // Ignore non-face anchors
    if (anchor is! ARKitFaceAnchor) return;

    // Launch music :D
    if (musicEnabled) player2.play().whenComplete(() => player2.play());

    faceNode = ARKitNode(geometry: anchor.geometry);
    arkitController.add(faceNode!, parentNodeName: anchor.nodeName);
  }

  void _handleUpdateAnchor(ARKitAnchor anchor) {
    // Get the orientation data, map to 3D and update pointer
    final Vector4 data = anchor.transform.getColumn(_faceOrientation);
    final Vector3 orientation = Vector3(data.x, data.y, data.z);
    _updatePointer(orientation);

    // Check for blinking
    if (anchor is ARKitFaceAnchor) {
      if (cooldown <= 0) {
        double leftEyeBlink = anchor.blendShapes['eyeBlink_L'] ?? 0;
        double rightEyeBlink = anchor.blendShapes['eyeBlink_R'] ?? 0;

        //music implementation
        if (musicEnabled) {
          if (leftEyeBlink > 0.9 && rightEyeBlink > 0.9 && blinkCounter >= 20) {
            player2.setUrl(musics[(musicCounter + 1) % musics.length]);
            musicCounter += 1;
            cooldown = 100;
            blinkCounter = 0;
          } else {
            blinkCounter++;
          }
          if (leftEyeBlink < 0.2 && rightEyeBlink < 0.2) {
            blinkCounter = 0;
          }
        }

        if ((leftEyeBlink > 0.8 && rightEyeBlink < 0.4) ||
            (rightEyeBlink > 0.8 && leftEyeBlink < 0.4)) {
          setState(() {
            cooldown = 100;
            drawingData.value =
                drawingData.value.withItem2(!drawingData.value.item2);
          });
          _plotterConnector.enableDrawing(drawingData.value.item2);
        }
      } else {
        cooldown -= 1;
      }

      // Check for mouth open
      double jawOpen = anchor.blendShapes['jawOpen'] ?? 0;

      if (jawOpen > 0.7) {
        if (!drawingData.value.item3) {
          drawingData.value = drawingData.value.withItem3(true);
        }
      }
      if (jawOpen <= 0.7 && drawingData.value.item3) {
        drawingData.value = drawingData.value.withItem3(false);
      }

      // Determine visibility of anchor
      if (faceDetected != anchor.isTracked) {
        setState(() {
          faceDetected = anchor.isTracked;

          if (!faceDetected) {
            drawingData.value = drawingData.value.withItem2(false);
          }
        });
        if (faceDetected) player.play().then((v) => player.seekToPrevious());
      }
    }
  }

  void _updatePointer(Vector3 orientation) async {
    // Map orientation to screen coordinates, reflect orientation along center
    Vector3 newCoordinates =
        await arkitController.projectPoint(orientation) ?? Vector3.zero();

    // Verify values here
    _pointerLocation.value = Offset(newCoordinates.x, newCoordinates.y);

    // // Send coordinates to plotter
    // double x = newCoordinates.x /
    //     (window.physicalSize.width / window.devicePixelRatio) *
    //     4;
    // double y = newCoordinates.y /
    //     (window.physicalSize.height / window.devicePixelRatio) *
    //     7;
    // _plotterConnector.setXY(x, y);
  }

  Offset getPointerLocation() {
    return _pointerLocation.value;
  }
}

class PointerPainter extends CustomPainter {
  static const double _pointerDiameter = 10;
  static final Paint _pointerPaint = Paint()..color = Colors.red;

  Offset previousPointerPosition = Offset.infinite;

  // Face canvas instance to access the pointer location
  late final FaceCanvasPageState _faceCanvas;
  late final ValueNotifier<Tuple3<Offset, bool, bool>> _drawingData;
  late final ValueNotifier<Tuple2<double, double>> _plotterValues;

  final mVX = MovingAverage<double>(
    averageType: AverageType.simple,
    windowSize: X_RATIO * FLOATING_WINDOW_SIZE,
    partialStart: true,
    getValue: (num n) => n,
    add: (List<double> data, num value) => (double.parse(value.toString())),
  );

  final mVY = MovingAverage<double>(
    averageType: AverageType.simple,
    windowSize: Y_RATIO * FLOATING_WINDOW_SIZE,
    partialStart: true,
    getValue: (num n) => n,
    add: (List<num> data, num value) => double.parse(value.toString()),
  );

  final ListQueue<double> _pointsX = ListQueue<double>(50);
  final ListQueue<double> _pointsY = ListQueue<double>(50);

  PointerPainter(
      {required FaceCanvasPageState faceCanvas,
      required Listenable repaint,
      required ValueNotifier<Tuple3<Offset, bool, bool>> drawingData,
      required ValueNotifier<Tuple2<double, double>> plotterValues})
      : super(repaint: repaint) {
    _faceCanvas = faceCanvas;
    _drawingData = drawingData;
    _plotterValues = plotterValues;
  }

  Offset _calculateEffectiveCoordinates(Size size) {
    Offset o = _faceCanvas.getPointerLocation();
    Offset center = size.center(Offset.zero);

    // Calculate the position of the pointer with respect to the center
    Offset positionWithCenter = -o.scale(2, 2) + center.scale(3, 3);

    if (o.dy < center.dy) {
      // Case where the pointer is under the center -- move it even more down
      positionWithCenter = Offset(positionWithCenter.dx,
          positionWithCenter.dy + OPTIMAL_COEFF * (center.dy - o.dy));
    } else {
      // Case where the pointer is above the center -- move it even more up
      positionWithCenter = Offset(
          positionWithCenter.dx, positionWithCenter.dy - (o.dy - center.dy));
    }
    if (positionWithCenter.dx < 0) {
      // If pointer is out of bounds on the left, move it to the left edge
      positionWithCenter = Offset(0, positionWithCenter.dy);
    }
    if (positionWithCenter.dy < 0) {
      // If pointer is out of bounds on the top, move it to the top edge
      positionWithCenter = Offset(positionWithCenter.dx, 0);
    }
    if (positionWithCenter.dx > size.width) {
      // If pointer is out of bounds on the right, move it to the right edge
      positionWithCenter = Offset(size.width, positionWithCenter.dy);
    }
    if (positionWithCenter.dy > size.height) {
      // If pointer is out of bounds on the bottom, move it to the bottom edge
      positionWithCenter = Offset(positionWithCenter.dx, size.height);
    }

    _pointsX.addLast(positionWithCenter.dx);
    _pointsY.addLast(positionWithCenter.dy);
    if (_pointsX.length > 50) {
      _pointsX.removeFirst();
      _pointsY.removeFirst();
    }

    double newPositionX = mVX(_pointsX.toList()).last;
    double newPositionY = mVY(_pointsY.toList()).last;
    positionWithCenter = Offset(newPositionX, newPositionY);
    previousPointerPosition = positionWithCenter;
    return positionWithCenter;
  }

  //method who:
  // 1. gets the pointer location from faceCanvas
  // 2. calculates the effective coordinates
  // 3. paints the pointer

  @override
  void paint(Canvas canvas, Size size) {
    // Paint requested. Get pointer location from faceCanvas and paint.
    sleep(const Duration(milliseconds: 10));
    //clamp size to 4:7 ratio
    size = Size(size.height * 4 / 7, size.height);
    Offset pointer = _calculateEffectiveCoordinates(size);

    // Send data to the plotter

    double x =
        pointer.dx / (window.physicalSize.width / window.devicePixelRatio) * 4;
    double y =
        pointer.dy / (window.physicalSize.height / window.devicePixelRatio) * 7;

    y = 7 - y;

    _plotterValues.value = Tuple2(x, y);

    Rect r = Rect.fromCenter(
        center: pointer, width: _pointerDiameter, height: _pointerDiameter);

    canvas.drawOval(r, _pointerPaint);

    // Notify the painting canvas
    _drawingData.value = _drawingData.value.withItem1(pointer);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
