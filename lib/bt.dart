import 'package:flutter/foundation.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';

class FacePlotter {
  static const facePlotterAddress =
      DeviceIdentifier("07F60FC8-4E60-9ED3-90FC-04324E6F215F");

  static final facePlotterServiceAddress =
      Guid("4fafc201-1fb5-459e-8fcc-c5c9c331914b");

  static final xCharacteristicAddress =
      Guid("beb5483e-36e1-4688-b7f5-ea07361b26a8");
  static final yCharacteristicAddress =
      Guid("ba7ff775-e5d6-4535-ae2d-70dfa683f250");
  static final activeCharacteristicAddress =
      Guid("25a6492e-f044-4021-8de0-3255f82a4545");

  final FlutterBluePlus _bluetooth = FlutterBluePlus.instance;

  late BluetoothDevice _plotter;
  late BluetoothCharacteristic _xProperty;
  late BluetoothCharacteristic _yProperty;
  late BluetoothCharacteristic _drawingEnabledProperty;

  final _connected = ValueNotifier<bool>(false);

  DateTime? _lastCallTime;

  FacePlotter() {
    connect();
  }

  void connect() async {
    // Connect to the plotter
    _bluetooth
        .startScan(timeout: const Duration(seconds: 4))
        .then((value) async {
      if (!_connected.value) {
        print("No plotter found");
        await _bluetooth.stopScan();
        connect();
      }
    });
    print("Scanning…");

    _bluetooth.scanResults.listen((res) async {
      for (ScanResult r in res) {
        if (r.device.name == "FaceDoodle Plotter") {
          _plotter = r.device;
          print("Found plotter");
          r.device.connect();
          await discoverServices();
          _bluetooth.stopScan();
          _connected.value = true;
          return;
        }
      }
    });
  }

  void disconnect() async {
    _plotter.disconnect();
    _connected.value = false;
  }

  Future<void> discoverServices() async {
    List<BluetoothService> services = await _plotter.discoverServices();
    for (BluetoothService service in services) {
      if (service.uuid == facePlotterServiceAddress) {
        print("Plotter service found");
        for (BluetoothCharacteristic characteristic
            in service.characteristics) {
          if (characteristic.uuid == xCharacteristicAddress) {
            _xProperty = characteristic;
          } else if (characteristic.uuid == yCharacteristicAddress) {
            _yProperty = characteristic;
          } else {
            _drawingEnabledProperty = characteristic;
          }
        }
      }
    }
  }

  ValueListenable<bool> connectedProperty() {
    return _connected;
  }

  void setXY(double x, double y) {
    if (!_connected.value) return;

    var now = DateTime.now();
    if (_lastCallTime != null) {
      var difference = now.difference(_lastCallTime!);
      if (difference.inMilliseconds < 200) {
        return;
      }
    }
    _lastCallTime = now;
    _xProperty.write(x.toString().codeUnits);
    _yProperty.write(y.toString().codeUnits);
  }

  void enableDrawing(bool enabled) {
    if (!_connected.value) return;
    _drawingEnabledProperty.write((enabled ? 1 : 0).toString().codeUnits);
  }
}
